from django.db import models

import pytz


class Subscriber(models.Model):

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    cell = models.DecimalField(max_digits=11, decimal_places=0, unique=True)
    cell_provider = models.ForeignKey(
        'CellProvider',
        on_delete=models.CASCADE,
        related_name='subscriber'
    )
    tag = models.ManyToManyField(
        'Tag',
        blank=True,
        related_name='subscriber',
    )
    # timezone = models.DecimalField(max_digits=2, decimal_places=0)
    timezone = models.CharField(max_length=32, choices=TIMEZONES)


class Mailing(models.Model):
    # format: YYYY-MM-DDThh:mm:ss
    start_date = models.DateTimeField('Дата начала рассылки')
    end_date = models.DateTimeField('Дата окончания рассылки')
    text = models.TextField()
    cell_provider = models.ManyToManyField('CellProvider', blank=True)
    # tag = models.CharField(max_length=20, blank=True)
    # format: YYYY-MM-DDThh:mm:ss


class Message(models.Model):
    OK = 'OK'
    ERROR = 'ERR'
    NOT_SENT = 'NO'
    SENDING_STATUSES = [
        (OK, 'Successfully sent'),
        (ERROR, 'Error while sending; need to retry'),
        (NOT_SENT, 'Not sent; need to send during next mailing session'),
    ]
    send_date = models.DateTimeField('Дата отправки сообщения')
    status = models.CharField(max_length=32, choices=SENDING_STATUSES)
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name='messages'
    )
    subscriber = models.ForeignKey(
        Subscriber,
        on_delete=models.CASCADE,
        related_name='messages'
    )

    class Meta:
        unique_together = ['mailing', 'subscriber']


class CellProvider(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name
