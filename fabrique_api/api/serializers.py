from rest_framework import serializers
# from rest_framework.relations import SlugRelatedField


from mailing.models import Mailing, Subscriber, Message, CellProvider, Tag


class MailingSerializer(serializers.ModelSerializer):
    cell_provider = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=CellProvider.objects.all()
    )
    tag = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Tag.objects.all(),
        required=False
    )

    class Meta:
        model = Mailing
        fields = 'id', 'start_date', 'end_date', 'text', 'tag', 'cell_provider'  # TODO delete id


class SubscriberSerializer(serializers.ModelSerializer):
    cell_provider = serializers.SlugRelatedField(
        slug_field='name',
        queryset=CellProvider.objects.all()
    )
    tag = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Tag.objects.all(),
        required=False
    )

    class Meta:
        model = Subscriber
        fields = 'id', 'cell', 'cell_provider', 'tag', 'timezone'  # TODO delete id


# class MessageSerializer(serializers.ModelSerializer):
#     author = serializers.SlugRelatedField(
#         read_only=True, slug_field='username'
#     )
#
#     class Meta:
#         model = Message
#         fields = '__all__'
#
#
# class FollowSerializer(serializers.ModelSerializer):
#     following = SlugRelatedField(
#         slug_field='username',
#         queryset=User.objects.all()
#     )
#     user = SlugRelatedField(
#         slug_field='username',
#         queryset=User.objects.all(),
#         default=serializers.CurrentUserDefault()
#     )
#
#     class Meta:
#         model = Follow
#         fields = ('following', 'user')
#
#     def validate(self, data):
#         if data.get('user') == data.get('following'):
#             raise serializers.ValidationError(
#                 'Can\'t follow yourself'
#             )
#         return data
