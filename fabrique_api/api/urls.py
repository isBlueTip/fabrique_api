from django.urls import include, path

from rest_framework.routers import SimpleRouter

from .views import (
    SubscriberViewSet,
    MailingViewSet,
)

router = SimpleRouter()

router.register('subscriber', SubscriberViewSet, basename='subscriber')
router.register('mailing', MailingViewSet)
# router.register(
#     r'posts/(?P<post_id>\d+)/comments',
#     CommentViewSet, basename='comment'
# )

urlpatterns = [
    path('v1/', include(router.urls)),
    path('v1/', include('djoser.urls.jwt')),
]
